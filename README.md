uC-R
===============

Copyright 2024 molelord

RenesasのRなんとか系マイコンで遊ぶためのプロジェクトです。

AがRA用、XがRX用のソース格納フォルダーです。気が向いたらL(RL78用)も作るかもしれません。

ソースのほか、Wikiにトラブルシューティング情報があります。

- [RA用Wiki](https://gitlab.com/ashidan/uc-r/-/wikis/RaTop)
- [RX用Wiki](https://gitlab.com/ashidan/uc-r/-/wikis/RxTop)

改行および文字コードは、自分で作ったファイルはLFかつUTF-8BOM無し、e2studioなどが自動生成したファイルは作られたときのまま、にしています。

ut/ はマイコン非依存のユニットテストです。ユニットテストの動作環境については、 ut/CMakeLists.txt に記載しています。

## 命名規則

| 対象 | 規則 | 備考 |
| ---- | ---- | ---- |
| 構造体 | `Foo` | |
| typedef名 | `〜_td` | `_t` は処理系で予約されているため |
| グローバル変数 | `g_foo_bar` | |
| ファイルスコープの変数 | `f_foo` | |
| クラスのメンバー変数 | `m_foo` | |
| クラスの静的メンバー変数 | `s_foo` | |
| マクロ | `MOLE_FOO_BAR` | 大文字をアンダーバーで連結 |
| インクルードガード | `FILENAME_HPP` | |

Eclipse(e2 studio)の Code Style - Formatter は、 BSD/Allman をもとに以下のカスタマイズをして使用しています。

- Tab policyをSpace only
- Indentの □Statements with switch body をチェック

## RXでのコンパイル時警告

全体に効くように-Wall -Wextraすると、Renesasのライブラリに対して警告が出まくって警告を読む気を無くします。
そこで、Compiler CPP - Source - User defined compiler optionsへ `-Wall` `-Wextra` `-Weffc++` を与えることで、自作のC++ソースにのみ警告が出るようにしています。

ほか、GCCRXのスタック消費量警告はあまり意味がない(タスクのスタック消費量は、関数1個よりも関数呼び出しの深さで決まるので、実行時に調べるしかない)、256に設定しています。

## RAでのコンパイル時警告

e2studioのSettingsダイアログで設定できるものを、テキトウに有効にしてあります。

/***********************************************************************
*
*  FILE        : kaede_frtos_pp.c
*  DATE        : 2024-03-15
*  DESCRIPTION : Main Program
*
*  NOTE:THIS IS A TYPICAL EXAMPLE.
*
***********************************************************************/
#include "FreeRTOS.h"
#include "task.h"

void main_task(void *pvParameters)
{

    /* Create all other application tasks here */

    while(1) {
        vTaskDelay(10000);
    }

    vTaskDelete(NULL);

}
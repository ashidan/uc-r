#!/bin/sh
# @copyright 2024 molelord
#
# 自動生成ソースなどを対象として小細工をする
# C/C++ Build - Settings の Build Stepsタブで、
# Pre-build steps の Command(s): に
# sh.exe ../buildsteps.sh pre
# Post-build steps の Command(s): に
# sh.exe ../buildsteps.sh post
# と記述することで、このスクリプトは実行される。
# (sh.exeが起動されたときのカレントディレクトリは
# RAなら Debug RXなら HardwareDebug である)
#
# Windowsのe2studioからだと、
# plugins/com.renesas.ide.exttools.busybox.win32.x86_64_1.3.6.v20230615-0931/bin
# にある cat cp echo mv rm sed sh xargs が利用できるようだ。
#
# 注意: なぜかRXではRAと挙動が違って、Post-build stepsのほうに記述した
# コマンドが「ビルドで何も更新する必要がなかった場合も」呼び出されるようだ。

#echo $PWD
#echo $PATH

function prebuild() {
    # Importのときに0 = RTOS is not usedにされるので、1 = FreeRTOS is usedに戻す
    target='src/smc_gen/r_config/r_bsp_config.h'

    # 余計なコンパイルが発生しないように、置換が成功した場合のみファイルを上書きする
    # grepが「文字列が見つからない」で終了する場合はすでにFreeRTOS is used
    # で何もする必要がなかったときである。
    grep -q '#define BSP_CFG_RTOS_USED.*0' < $target && \
    sed -i -e s/'\(#define BSP_CFG_RTOS_USED.*\)0'/'\11'/ $target

    # FreeRTOSソースの存在を調べて、存在すればこのスクリプトとして成功を
    # 返すが、無いならメッセージを出して失敗を返す。
    if [ -d src/FreeRTOS/Source ]; then
        true
    else
        echo 'Error: copy src/FreeRTOS/Source from other e2studio project'
        false
    fi
}

function postbuild() {
    if [ x"${MSYS2PATH}" != x"" ]; then
        # libmoleのユニットテストを実行
        # uC-Rのユニットテストを実行
        true
    fi
}

cd ..

# MSYS2のインストールフォルダを探す
# ただし、環境変数MSYS2PATHが設定済みであればそれを優先する
if [ x"${MSYS2PATH}" = x"" ]; then
    if [ -d c:/msys64 ]; then
        MSYS2PATH=c:/msys64
    elif [ -d c:/i/msys64 ]; then
        MSYS2PATH=c:/i/msys64
    fi
fi

if [ x"${1}" = x"pre" ]; then
    prebuild
elif [ x"${1}" = x"post" ]; then
    postbuild
else
    echo unknown step
    false
fi

/**
 * @file
 * @brief ほかのタスクを起動する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#include "FreeRTOS.h"
#include "task.h" // vTaskDelay()
#include "timers.h" // TimerHandle_t
#include "libmole/mole_misc.hpp" // elementsOf()
#include "libmole/mole_trap.h" // MOLE_ALWAYS()

#define APP_PCDC0_UART1 0

#if APP_PCDC0_UART1 == 1
#   include "libmole/mole_Uart.hpp" // Uart
#else
#   include "libmole/mole_PcdcUart.hpp" // PcdcUart
#   include "libmole/mole_pcdc_desc.h" // mole_pcdc_setVidPid()
#endif

#include "libmole/mole_Shell.hpp" // Shell
#include "libmole/mole_RxPin.hpp" // RxPin
#include "libmole/mole_misc.hpp" // MOLE_ESC_*
#include "libmole/mole_ZzForth.hpp" // ZzForth
#include "libmole/mole_i2c.hpp" // I2c
#include <cstdio> // snprintf()
#include <cstring> // strlen()

/* RXファミリ MPC モジュール Firmware Integration Technology
 * 1.3に記載された作法に従う。 */
extern "C"
{
#include "r_sci_rx_pinset.h" // R_SCI_PinSet_SCIn()
#include "r_usb_basic_if.h" // USB_PCDC
#include "r_usb_basic_pinset.h" // R_USB_PinSet_USB0_PERI()
#if defined(SCI_CFG_CH7_TX_DTC_DMACA_ENABLE) && SCI_CFG_CH7_TX_DTC_DMACA_ENABLE == 1
#   include "r_dtc_rx_if.h" // R_DTC_Open()
#endif
}

#define PROMPT MOLE_ESC_GREEN "> " MOLE_ESC_DEFAULT

namespace app {

static constexpr gpio_port_pin_t LED1 = GPIO_PORT_C_PIN_0;
static constexpr gpio_port_pin_t LED2 = GPIO_PORT_C_PIN_1;
static constexpr gpio_port_pin_t LED3 = GPIO_PORT_0_PIN_2;
static constexpr gpio_port_pin_t LED4 = GPIO_PORT_0_PIN_3;

static void blinkTimerCb(TimerHandle_t handle) noexcept
{
    (void)handle;
    static int32_t on1off0 = 0;
    R_GPIO_PinWrite(app::LED1,
        on1off0 ? GPIO_LEVEL_HIGH : GPIO_LEVEL_LOW);
    on1off0 ^= 0x1;
}

class SimpleInterpreter : public mole::Interpreter {
public:
    SimpleInterpreter() noexcept {}
    virtual ~SimpleInterpreter() noexcept {}

    // コマンド文字列の長さを表示するだけの簡易インタプリタ
    virtual void interpret(mole::ByteIo* io, const char* line) noexcept override
    {
        char buf[8];
        std::snprintf(buf, sizeof(buf), "%lu: ", std::strlen(line));
        io->write(buf);
        io->write(line);
        io->write("\r\n");
    }
};

}

static void app_launcher_tsk(void *pvParameters) noexcept
{
    (void)pvParameters;

    /* ここが実行されるとき、ほかにIdleとTimerのタスクが動作している。
     * 不確定な要素無しに初期化を行いたいので、IdleとTimer、それに加え
     * xTaskCreate()したタスクも実行を停止しておく。
     * ただし、割り込みは生きたまま。 */
    vTaskSuspendAll();

    //! @todo ここで各種の初期化を行う

    static constexpr mole::RxPin pin[] = {
        //                            irq ana hl  out
        {mole::RxPin::pack(app::LED1), 0 | 0 | 0 | 8},
        {mole::RxPin::pack(app::LED2), 0 | 0 | 0 | 8},
        {mole::RxPin::pack(app::LED3), 0 | 0 | 0 | 8},
        {mole::RxPin::pack(app::LED4), 0 | 0 | 0 | 8},
        /* 144ピンパッケージに存在しないピンをLow出力に設定する
         * RX64Mグループ 初期設定例 表3.2 より */
        {mole::RxPin::pack('1', 0),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('1', 1),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('9', 4),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('9', 5),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('9', 6),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('9', 7),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('F', 0),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('F', 1),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('F', 2),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('F', 3),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('F', 4),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 0),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 1),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 2),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 3),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 4),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 5),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 6),    0 | 0 | 0 | 8},
        {mole::RxPin::pack('G', 7),    0 | 0 | 0 | 8},
    };
    for (uint32_t i = 0; i < mole::elementsOf(pin); i++)
    {
        pin[i].commit();
    }

    R_SCI_PinSet_SCI7();
    R_USB_PinSet_USB0_PERI();

    // LED点滅はTimerタスクにやらせる
    {
        static StaticTimer_t timerBody;
        const TimerHandle_t handle = xTimerCreateStatic(
            "Blink",
            pdMS_TO_TICKS(500),
            pdTRUE,             // uxAutoReload
            static_cast<void*>(0), // pvTimerID
            app::blinkTimerCb,
            &timerBody);
        MOLE_ALWAYS(nullptr != handle, handle);
        const auto rc = xTimerStart(handle, 0);
        MOLE_ALWAYS(pdTRUE == rc, rc);
    }

#if defined(SCI_CFG_CH7_TX_DTC_DMACA_ENABLE) && SCI_CFG_CH7_TX_DTC_DMACA_ENABLE == 1
    // Uartコンストラクタ内でのR_Uart_Open()呼び出しに先立ち、R_DTC_Open()が必要
    R_DTC_Open();
#endif

    static mole::BufferImpl<128U> txRing;
    static mole::BufferImpl<128U> rxRing;
#if APP_PCDC0_UART1 == 1
    static constexpr sci_cfg_t cfg = {
        .async = {
            .baud_rate    = 115200U,
            .clk_src      = SCI_CLK_INT,
            .data_size    = SCI_DATA_8BIT,
            .parity_en    = SCI_PARITY_OFF,
            .parity_type  = SCI_EVEN_PARITY,
            .stop_bits    = SCI_STOPBITS_1,
			.int_priority = 2U, // TimerTick に+1した優先度
        },
    };
    mole::ByteIo* io = mole::UartFactory::create(
        SCI_CH7, &cfg,
        &txRing, &rxRing);
#endif
    // 参考 https://www.freertos.org/a00135.html
    if (!xTaskResumeAll())
    {
        taskYIELD();
    }

    // 以上で初期化処理は終わり。以下は通常処理。

#if APP_PCDC0_UART1 == 0
    /**
     * @todo 使用者の責任で、正式なベンダーIDとプロダクトIDに直す
     * 警告: このVIDとPIDの組み合わせは個人的な実験の範囲で使うため
     * のものであり、世の中に出す製品には使用してはいけない。
     * 参照 https://pid.codes/1209/0001/
     */
    mole_pcdc_setVidPid(0x1209U, 0x0001U);

    // 1台のPCにRAボードと一緒に挿した場合でも、iSerialNumberを衝突させないため
    mole_pcdc_setSerialNumber(2U);

    static const usb_ctrl_t usb = {
        .module = USB_IP0,
        .address = 0U,
        .pipe    = 0U,
        .type    = USB_PCDC,
        .status  = 0U,
        .event   = 0U,
        .size    = 0U,
        .setup   = {},
        .p_data  = nullptr,
    };
    extern const usb_descriptor_t g_usb_descriptor;
    static const usb_cfg_t usbcfg = {
        .usb_mode  = USB_PERI,
        .usb_speed = USB_FS,
        .p_usb_reg = const_cast<usb_descriptor_t*>(&g_usb_descriptor),
    };

    /* FreeRTOSスケジューラを停止させた状態でR_USB_Open()を呼ぶと、
     * R_USB_Open()の延長でvTaskDelay()が呼ばれてabort()する。
     * よって、xTaskResumeAll()したあとでPcdcUartインスタンスを
     * 作成する。 */
    mole::ByteIo* io = mole::PcdcUartFactory::create(
        &usb, &usbcfg,
        &txRing, &rxRing,
        3U);
#endif

#if 0
    // 単純な送受信のテスト
    for (int32_t i = 0;; i++)
    {
#if 0
        // 送信のテスト
        char msg[16];
        std::snprintf(msg, sizeof(msg), "%ld: ", i);
        io->write(msg);
#else
        /* 受信のテスト
         * TeraTermのメニューバーの、ファイル-ファイル送信 にて、
         * □バイナリをチェックした状態でテキトウなファイルを送って
         * みるとよい。 */
        uint8_t buf[16];
        size_t received = io->read(buf, sizeof(buf));
        io->write(buf, received);
#endif
    }
#endif

#if 1
#if 1
    // コマンド入力受付のテスト
    auto interp = mole::ZzForth(io);
#else
    auto interp = app::SimpleInterpreter();
#endif
    static constexpr uint32_t COMMAND_LEN_MAX = 127;
    static uint8_t line[COMMAND_LEN_MAX+1]; // +1はゼロ終端のぶん
    auto shell = mole::Shell(io, &interp, line, sizeof(line));

    shell.exec();
#endif

#if 0 // IIC実験
    // なぜか呼び出しのない src/smc_gen/r_pincfg/Pin.c からコピーしてきた処理
    {
        R_BSP_RegisterProtectDisable(BSP_REG_PROTECT_MPC);
        /* Set SCL0 pin */
        MPC.P12PFS.BYTE = 0x0FU;
        PORT1.PMR.BYTE |= 0x04U;
        PORT1.ODR0.BYTE |= (1U<<4U); //オープンドレイン化必要？

        /* Set SDA0 pin */
        MPC.P13PFS.BYTE = 0x0FU;
        PORT1.PMR.BYTE |= 0x08U;
        PORT1.ODR0.BYTE |= (1U<<6U); //オープンドレイン化必要？
        R_BSP_RegisterProtectEnable(BSP_REG_PROTECT_MPC);
    }

#if 0
    static constexpr uint8_t slvAddr = 0x7CU>>1;
    static constexpr uint8_t Co  = 0x80U;
    static constexpr uint8_t Dat = 0x40U;
    mole::I2c i2c(0U);

    struct X
    {
    	uint8_t  msg[2];
    	uint32_t waitms;
    };
    static const X x[] = {
        { {0x0U, 0x38U}, 1},
        { {0x0U, 0x39U}, 1},
		{ {0x0U, 0x14U}, 1},
		{ {0x0U, 0x70U}, 1},
		{ {0x0U, 0x56U}, 1},
		{ {0x0U, 0x6CU}, 1},
		{ {0x0U, 0x38U}, 1},
		{ {0x0U, 0x0CU}, 1},
		{ {0x0U, 0x01U}, 1}, // Clear Display
		//{ {0x0U, 0x02U}, 1}, // Return Home
		//{ {0x0U, 0x06U}, 1}, // Entry Mode Set
    };
    for (int32_t i = 0; i < mole::elementsOf(x); i++)
    {
    	i2c.send(slvAddr, x[i].msg, 2U);
        vTaskDelay(x[i].waitms);
    }

    static const uint8_t msg1[] = {0x0U, 0x80U}; // Set DDRAM Address
    i2c.send(slvAddr, msg1, sizeof(msg1));
    vTaskDelay(1);

    static const uint8_t msg2[] = {Dat, 'A'};
    i2c.send(slvAddr, msg2, sizeof(msg2));
#endif

    // heltec OLED
    static constexpr uint8_t slvAddr = 0x78>>1U;
    static constexpr uint8_t Co  = 0x80U;
    static constexpr uint8_t Dat = 0x40U;

    mole::I2c i2c(0U);

    struct X
    {
    	uint8_t  msg[2];
    	uint32_t waitms;
    };
    static const X x[] = {
        { {0x0U, 0xA7U}, 1000},
    };
    for (int32_t i = 0; i < mole::elementsOf(x); i++)
    {
    	i2c.send(slvAddr, x[i].msg, 2U);
        vTaskDelay(x[i].waitms);
    }

    for (;;)
    {
        vTaskDelay(1);
    }
#endif
}

/* MAIN_TASKのスタックサイズは変更不能で融通が効かないので、MAIN_TASKではなく
 * LauncherTskで初期化処理を行うこととし、MAIN_TASKのほうは即終了させる。 */
extern "C" void main_task(void *pvParameters)
{
    (void)pvParameters;

    const TaskHandle_t me = xTaskGetCurrentTaskHandle();

    /* MAIN_TASKが消費しているヒープをすぐに開放したいので、
     * 最高優先度にすることでLauncherThrより優先して実行させ、
     * 素早く終了させる。 */
    vTaskPrioritySet(me, configMAX_PRIORITIES - 1U);

    static StackType_t  stack[2048/sizeof(StackType_t)];
    static StaticTask_t tcb;
    const TaskHandle_t handle = xTaskCreateStatic(
        app_launcher_tsk,
        "LauncherTsk",
		mole::elementsOf(stack),
		nullptr,
		2U,
		stack,
		&tcb);
    MOLE_ALWAYS(nullptr != handle, handle);

    vTaskDelete(me);
}

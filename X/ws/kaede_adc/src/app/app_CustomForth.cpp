/**
 * @file
 * @brief zForthにユーザー関数を追加する実例
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "app/app_CustomForth.hpp"
#include "libmole/mole_trap.h" // mole_abort()
#include "zforth/zforth.h" // zf_init()
#include <cstdio> // snprintf()
#include <cstring> // strlen()

#define UF_SYSCALL_HOGEFUGA 0

namespace app {

/**
 * zForthのインスタンシエート時点で登録するワードの羅列。
 * 末尾の番兵を除き、各行を\0で終える必要がある。
 */
static const char* const f_words =
"hogefuga\0"
""; // 番兵

CustomForth::CustomForth(mole::ByteIo* io) noexcept : zForth(io, f_words)
{
}

void CustomForth::userFunc(mole::ByteIo* io, int32_t userFunc, const char* input) noexcept
{
    (void)input;
    MOLE_ASSERTNN(io);

    switch (userFunc)
    {
        case UF_SYSCALL_HOGEFUGA:
            {
                /* pop x2 push x1
                 * Usage: arg2 arg1 hofegufa
                 * arg2のarg1乗を求めて結果をpushする
                 * 例 2 8 hogefuga の結果は 256
                 */
                char buf[32];

                const zf_cell jyou = zf_pop();
                std::snprintf(buf, sizeof(buf),
                    "hogefuga 1starg: " ZF_CELL_FMT, jyou);
                io->write(buf);

                const zf_cell moto = zf_pop();
                std::snprintf(buf, sizeof(buf),
                    " 2ndarg: " ZF_CELL_FMT " result: ", moto);
                io->write(buf);

                zf_cell result = 1;
                for (int32_t i = 0; i < jyou; i++)
                {
                    result *= moto;
                }
                zf_push(result);
            }
            break;
        default:
            mole_abort(userFunc);
            break;
    }
}

}

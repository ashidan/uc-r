/**
 * @file
 * @brief zForthにユーザー関数を追加する実例
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef APP_CUSTOMFORTH_HPP
#define APP_CUSTOMFORTH_HPP

#include "libmole/mole_zForth.hpp" // zForth

namespace app {

class CustomForth : public mole::zForth {
public:
    /**
     * @brief コンストラクタ
     */
    CustomForth(mole::ByteIo* io) noexcept;

    CustomForth(const CustomForth&) = delete;
    CustomForth& operator=(const CustomForth&) = delete;
    CustomForth(CustomForth&& rhs) = delete;
    CustomForth& operator=(CustomForth&& rhs) = delete;

    virtual ~CustomForth() noexcept {}

    /**
     * @brief zForthのユーザー関数を実装する
     * @param io 入出力インスタンス
     * @param userFunc ユーザー関数番号 値域は128以上
     * @param input 不明
     */
    virtual void userFunc(mole::ByteIo* io, int32_t userFunc, const char* input) noexcept override;
};

}
#endif

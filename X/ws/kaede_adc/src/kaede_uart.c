#if 0
/***********************************************************************
*
*  FILE        : kaede_uart.c
*  DATE        : 2024-03-17
*  DESCRIPTION : Main Program
*
*  NOTE:THIS IS A TYPICAL EXAMPLE.
*
***********************************************************************/
#include "FreeRTOS.h"
#include "task.h"

void main_task(void *pvParameters)
{

    /* Create all other application tasks here */

    while(1) {
        vTaskDelay(10000);
    }

    vTaskDelete(NULL);

}
#endif

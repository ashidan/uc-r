注意 ここにはRenesasに著作権のあるファイルが置かれています。

- r_usb_pcdc_descriptor.c.template RA FSP5.2.0のr_usb_pdcdに付属のものを流用

以下は、「r_usb_pcdc_descriptor.c.templateから#includeされているものの、RXの環境には
存在しないヘッダ」をダミーとして用意したもので、Renesasの著作物ではありません。

- r_usb_basic.h
- r_usb_basic_api.h
- r_usb_basic_cfg.h

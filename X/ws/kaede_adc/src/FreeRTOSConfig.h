/**
 * @file
 * @brief FreeRTOSConfig.h で行われた定義を強引に上書きする
 * @details
 * RAのとは異なり、RXのsrc/frtos_config/FreeRTOSConfig.h は柔軟性が無い。
 * そこで、src/frtos_config のものではなくこのファイルを#includeの対象に
 * しておいて、このファイルからユーザーが用意したヘッダを#includeする。
 * 
 * この仕組みを機能させるには、
 * C/C++ Build - Settings - Tool Settings にて、Compiler - Includesの
 * リストから
 * ${workspace_loc:/${ProjName}/src/frtos_config}
 * の行を削除したうえで、リストのいちばん上に
 * ${workspace_loc:/${ProjName}/src}
 * を追加する必要がある。
 * 
 * #includeの入れ子の関係
 * @Cソース : #include "FreeRTOS.h"
 *   @FreeRTOS.h : #include "FreeRTOSConfig.h"
 *     @このヘッダ : #include "frtos_config/FreeRTOSConfig.h"
 *     @このヘッダ : #include "app/app_FreeRTOSConfig.h"
 *        @app/app_FreeRSOSConfig.h : #include "libmole/mole_FreeRTOSConfig.h"
 *
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef OVRWRITE_FREERTOSCONFIG_H
#define OVRWRITE_FREERTOSCONFIG_H

#include "frtos_config/FreeRTOSConfig.h"
#include "app/app_FreeRTOSConfig.h"

#endif

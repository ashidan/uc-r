RX
===============

Copyright 2024 molelord

RXマイコンの載ったボードで遊んでみます。

開発環境として、 https://github.com/renesas/fsp/releases から入手できる setup_fsp_v5_1_0_e2s_v2023-10.exe に、RX用の開発ツールを足した [手順](https://gitlab.com/ashidan/uc-r/-/wikis/RxTop) ものを使用しています。

```
ws/
  +- kaede_frtos_pp/ GR-KAEDEをC++かつFreeRTOSで動かす
  +- kaede_uart/     GR-KAEDEでUARTで [zForth](https://github.com/zevv/zForth) を動かす
  +- kaede_adc/     GR-KAEDEでADCを動かす ビルドは通るが未完成
```

## workspaceを復元する

プロジェクトをe2studioで扱える状態に復元するには、以下の手順に従ってください。

7と8の組み合わせで、別プロジェクトからFreeRTOSソースをコピーしています。
その理由は、FreeRTOSソースはuc-rリポジトリで版管理すべきでないと考え、リポジトリへ含めないようにしたためです。

 1. e2studioで、workspaceとして path/to/uc-r/X/ws を開く
 2. welcomeを閉じる
 3. File - Import - General - Existing Projects into Workspace
 4. Browse...
 5. 何も選択せず フォルダーの選択 ボタンをクリック
 6. Finish
 7. 任意の名前でGCC for Renesas RX C/C++ Executable Projectを作る
   - RTOSはFreeRTOS(kernel only)、RTOS Versionは10.4.3-rx-1.0.6
 8. 任意の名前/src/FreeRTOS が作られるので、それを kaede_uart/src および kaede_frtos_pp/src へコピーする
 9. Project - Build All
10. ビルドが成功したら、任意の名前 プロジェクトを削除する


@echo off
rem Copyright 2024 molelord
setlocal

rem この4行を変えるだけでだいたい何とかなるはず
set vendor=molelord
set board=RA4M1_MINIMA
set fspmn=5.2
set fspver=%fspmn%.0
set ecli=C:\Users\%USERNAME%\.eclipse

set target=%vendor%.%board%

rem x.y.zの部分をFSPバージョンに変えたファイルを用意する
pushd %target%.x.y.z
copy .module_descriptions\%vendor%##BSP##Board##%board%####x.y.z##configuration.xml .module_descriptions\%vendor%##BSP##Board##%board%####%fspver%##configuration.xml
copy .module_descriptions\%vendor%##BSP##Board##%board%####x.y.z.xml .module_descriptions\%vendor%##BSP##Board##%board%####%fspver%.xml
popd

pushd %ecli%
rem e2studioにてpackを既にImport済みなら、それを削除する
for /d %%i in (com.renesas.platform_*) do (
  if exist %%i\internal\projectgen\ra\%fspmn% (
    pushd %%i\internal\projectgen\ra
    del packs\%vendor%.%board%.%fspver%.pack 2> nul
    del modules\%vendor%##BSP##Board##%board%####%fspver%##configuration.xml 2> nul
    del modules\%vendor%##BSP##Board##%board%####%fspver%.xml 2> nul
    popd
  )
)
popd

powershell compress-archive -Force %target%.x.y.z/* %target%.%fspver%.zip
del %target%.%fspver%.pack 2> nul

rem 時間待ちせずすぐrenすると、ファイルが使用中というエラーになってしまう
timeout /T 1

ren %target%.%fspver%.zip %target%.%fspver%.pack

endlocal

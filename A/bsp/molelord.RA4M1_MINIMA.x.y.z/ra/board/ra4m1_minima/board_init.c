/* 参考 https://en-support.renesas.com/knowledgeBase/20025489 
 * 参考 EK-RA6M4向けにプロジェクトを作成したときの
 * ra/board/ra6m4_ek/board_init.c
 */
#include "bsp_api.h"

#if defined(BOARD_RA4M1_MINIMA)

void bsp_init(void* p_args)
{
    FSP_PARAMETER_NOT_USED(p_args);
}

#endif

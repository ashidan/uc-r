/* 参考 https://en-support.renesas.com/knowledgeBase/20025489 
 * 参考 EK-RA6M4向けにプロジェクトを作成したときの
 * ra/board/ra6m4_ek/board_init.h
 */
#ifndef BOARD_INIT_H
#define BOARD_INIT_H
#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief ボード固有の初期化を行う？
 * @param[in] p_args どこの何が渡されるのか不明
 */
void bsp_init(void* p_args);

#ifdef __cplusplus
}
#endif
#endif

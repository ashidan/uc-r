/* 参考 https://en-support.renesas.com/knowledgeBase/20025489 
 * 参考 EK-RA6M4向けにプロジェクトを作成したときの
 * ra_cfg/fsp_cfg/bsp/board.h
 */
#ifndef BOARD_H
#define BOARD_H

#include "board_init.h"
#include "board_leds.h"

#define BOARD_RA4M1_MINIMA 1

#endif

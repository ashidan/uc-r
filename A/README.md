RA
===============

Copyright 2024 molelord

Arduino UNO R4 Minima を、ArduinoボードとしてでなくRAマイコンの評価ボードとして使おうという試みです。

開発環境として、 https://github.com/renesas/fsp/releases から入手できる setup_fsp_v5_1_0_e2s_v2023-10.exe を使用しています。

```
ws/
  +- minima_bsp/ R4 Minima用Board Support Packageを作る
  +- minima_uart/ R4 MinimaでUARTで [zForth](https://github.com/zevv/zForth) を動かす
  +- ckra_uart/ CK-RA6M5 v2でUARTでzForthを動かす
```

## Arduino Minima用のBoard Support Packageを導入する

workspaceの復元の前に、ws/minima_uart から依存されているBSPを導入する必要があります。

1. bsp/makepack.cmd をダブルクリック molelord.RA4M1_MINIMA.?.?.?.pack ができる
2. e2studioを起動し、任意のworkspaceを開く
3. e2studioのFile - Import
4. General - CMSIS Pack を選択しNext
5. Specify pack file: に、先程作成した〜.packを指定
6. Specify device family: に、Renesas RA を指定しFinish

## workspaceを復元する

プロジェクトをe2studioで扱える状態に復元するには、以下の手順に従ってください。

 1. e2studioで、workspaceとして path/to/uc-r/A/ws を開く
 2. welcomeを閉じる
 3. File - Import - General - Existing Projects into Workspace
 4. Browse...
 5. 何も選択せず フォルダーの選択 ボタンをクリック
 6. Finish
 7. Project - Build All

## 余談
[RA4M1](https://www.renesas.com/jp/ja/products/microcontrollers-microprocessors/ra-cortex-m-mcus/ra4m1-32-bit-microcontrollers-48mhz-arm-cortex-m4-and-lcd-controller-and-cap-touch-hmi) はTrustZone非搭載のCortex-M4なので、考えなきゃならないことが減って気が楽です。

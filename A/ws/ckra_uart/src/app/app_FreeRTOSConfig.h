/**
 * @file
 * @brief FSP Configuration上でCustom FreeRTOSConfig.h に指定する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#ifndef APP_FREERTOSCONFIG_H
#define APP_FREERTOSCONFIG_H
#include "libmole/mole_FreeRTOSConfig.h"
#endif

/**
 * @file
 * @brief ほかのタスクを起動する
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */
#include <app_launcher_tsk.h>
#include "timers.h" // TimerHandle_t
#include "libmole/mole_trap.h" // MOLE_ALWAYS()

#define APP_PCDC0_UART1 0

#if APP_PCDC0_UART1 == 1
#   include "libmole/mole_Uart.hpp" // Uart
#else
#   include "libmole/mole_PcdcUart.hpp" // PcdcUart
#   include "libmole/mole_pcdc_desc.h" // mole_pcdc_setVidPid()
#endif

#include "libmole/mole_Shell.hpp" // Shell
#include "libmole/mole_ZzForth.hpp" // ZzForth
#include "libmole/mole_misc.hpp" // MOLE_ESC_*
#include <cstdio> // snprintf()
#include <cstring> // strlen()

#define PROMPT MOLE_ESC_GREEN "> " MOLE_ESC_DEFAULT

extern const bsp_leds_t g_bsp_leds; //!< ra/board/ra6m5_ck_v2/board_leds.c にある

namespace app {

static void blinkTimerCb(TimerHandle_t handle) noexcept
{
    (void)handle;
    static bsp_io_level_t pin_level = BSP_IO_LEVEL_LOW;

    /* Update all board LEDs */
    for (uint32_t i = 0; i < g_bsp_leds.led_count; i++)
    {
        const uint32_t pin = g_bsp_leds.p_leds[i];
        R_BSP_PinWrite((bsp_io_port_pin_t)pin, pin_level);
    }

    pin_level = (BSP_IO_LEVEL_LOW == pin_level) ? BSP_IO_LEVEL_HIGH : BSP_IO_LEVEL_LOW;
}

class SimpleInterpreter : public mole::Interpreter {
public:
    SimpleInterpreter() noexcept {}
    virtual ~SimpleInterpreter() noexcept {}

    // コマンド文字列の長さを表示するだけの簡易インタプリタ
    virtual void interpret(mole::ByteIo* io, const char* line) noexcept override
    {
        char buf[8];
        std::snprintf(buf, sizeof(buf), "%d: ", std::strlen(line));
        io->write(buf);
        io->write(line);
        io->write("\r\n");
    }
};

}

extern "C" void app_launcher_tsk_entry(void *pvParameters)
{
    (void)pvParameters;

    /* ここが実行されるとき、ほかにIdleとTimerのタスクが動作している。
     * 不確定な要素無しに初期化を行いたいので、IdleとTimer、それに加え
     * xTaskCreate()したタスクも実行を停止しておく。
     * ただし、割り込みは生きたまま。 */
    vTaskSuspendAll();

    R_BSP_PinAccessEnable();

    // LED点滅はTimerタスクにやらせる
    {
        static StaticTimer_t timerBody;
        const TimerHandle_t handle = xTimerCreateStatic(
            "Blink",
            pdMS_TO_TICKS(500),
            pdTRUE,             // uxAutoReload
            static_cast<void*>(0), // pvTimerID
            app::blinkTimerCb,
            &timerBody);
        MOLE_ALWAYS(nullptr != handle, handle);
        const auto rc = xTimerStart(handle, 0);
        MOLE_ALWAYS(pdTRUE == rc, rc);
    }

    static mole::BufferImpl<128U> txRing;
    static mole::BufferImpl<128U> rxRing;
#if APP_PCDC0_UART1 == 1
    mole::ByteIo* io = mole::UartFactory::create(&g_uart_ardu,
        &txRing, &rxRing);
#endif
    // 参考 https://www.freertos.org/a00135.html
    if (!xTaskResumeAll())
    {
        taskYIELD();
    }

    // 以上で初期化処理は終わり。以下は通常処理。

#if APP_PCDC0_UART1 == 0
    /**
     * @todo 使用者の責任で、正式なベンダーIDとプロダクトIDに直す
     * 警告: このVIDとPIDの組み合わせは個人的な実験の範囲で使うため
     * のものであり、世の中に出す製品には使用してはいけない。
     * 参照 https://pid.codes/1209/0001/
     */
    mole_pcdc_setVidPid(0x1209U, 0x0001U);

    /* FreeRTOSスケジューラを停止させた状態でR_USB_Open()を呼ぶと、
     * R_USB_Open()の延長でvTaskDelay()が呼ばれてabort()する。
     * よって、xTaskResumeAll()したあとでPcdcUartインスタンスを
     * 作成する。 */
    mole::ByteIo* io = mole::PcdcUartFactory::create(
        &g_basic0, &txRing, &rxRing);
#endif

#if 0
    // 単純な送受信のテスト
    for (int32_t i = 0;; i++)
    {
#if 0
        // 送信のテスト
        char msg[16];
        std::snprintf(msg, sizeof(msg), "%ld: ", i);
        io->write(msg);
        static const char* const buf = "abcdefghijk!?\r\n";
        io->write(buf);
#else
        /* 受信のテスト
         * TeraTermのメニューバーの、ファイル-ファイル送信 にて、
         * □バイナリをチェックした状態でテキトウなファイルを送って
         * みるとよい。 */
        uint8_t buf[16];
        size_t received = io->read(buf, sizeof(buf));
        io->write(buf, received);
#endif
    }

#else
    // コマンド入力受付のテスト

#if 1
    auto interp = mole::ZzForth(io);
#else
    auto interp = app::SimpleInterpreter();
#endif
    static constexpr uint32_t COMMAND_LEN_MAX = 127;
    uint8_t line[COMMAND_LEN_MAX+1]; // +1はゼロ終端のぶん
    auto shell = mole::Shell(io, &interp, line, sizeof(line));

    shell.exec();
#endif
}

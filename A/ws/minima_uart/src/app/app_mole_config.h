/**
 * @file
 * @brief libmoleの持つ機能の取捨選択をする
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 * @see https://gitlab.com/ashidan/libmole/-/blob/main/src/libmole/mole_config.h
 * @note
 * RAでUSB PCDC UARTを使用する場合、R_USB_Open()のなかで"PCD_TSK"という
 * 名前のFreeRTOS Taskが生成される。
 * その関係で、FreeRTOSヒープ
 * (FSP Configurationの Common - Memory Allocation - Total Heap Size)
 * が小さすぎるときはにR_USB_Open()が失敗でリターンする。
 */
#ifndef APP_MOLE_CONFIG_H
#define APP_MOLE_CONFIG_H

#ifndef MOLE_CONFIG1
//                      ? -+ +- ?
//                    ? -+ | | +- ?
//                  ? -+ | | | | +- UART
//                     | | | | | |
#define MOLE_CONFIG1  000000000011U
//                      | | | | |
//                   ? -+ | | | +- USB PCDC UART
//                     ? -+ | +- ?
//                          +- ?
#endif

#endif

#!/bin/sh
# @copyright 2024 molelord
#
# 自動生成ソースなどを対象として小細工をする
# C/C++ Build - Settings の Build Stepsタブで、
# Pre-build steps の Command(s): に
# sh.exe ../buildsteps.sh pre
# Post-build steps の Command(s): に
# sh.exe ../buildsteps.sh post
# と記述することで、このスクリプトは実行される。
# (sh.exeが起動されたときのカレントディレクトリは
# RAなら Debug RXなら HardwareDebug である)
#
# Windowsのe2studioからだと、
# plugins/com.renesas.ide.exttools.busybox.win32.x86_64_1.3.6.v20230615-0931/bin
# にある cat cp echo mv rm sed sh xargs が利用できるようだ。
#
# 注意: Pre-build stepsのほうは無条件で呼び出されるが、Post-build stepsの
# ほうに記述したコマンドは「ビルドで何か更新する必要が発生した場合」にしか
# 呼び出されないようだ。

#echo $PWD
#echo $PATH

function prebuild() {
    return 0
}

function postbuild() {
    if [ x"${MSYS2PATH}" != x"" ]; then
        # libmoleのユニットテストとDoxygenを実行
        MSYSTEM=CLANG32 MSYS=winsymlinks:nativestrict ${MSYS2PATH}/usr/bin/bash.exe --login -c "cd ../../../sub/libmole/ut && cmake -S . -B build && cmake --build build -t ut_all -t doc && ./build/ut_all -c"

        # uC-Rのユニットテストを実行
        MSYSTEM=CLANG32 MSYS=winsymlinks:nativestrict ${MSYS2PATH}/usr/bin/bash.exe --login -c "cd ../../../ut && cmake -S . -B build && cmake --build build && ./build/ut_all -c"
    fi
}

cd ..

# MSYS2のインストールフォルダを探す
# ただし、環境変数MSYS2PATHが設定済みであればそれを優先する
if [ x"${MSYS2PATH}" = x"" ]; then
    if [ -d c:/msys64 ]; then
        MSYS2PATH=c:/msys64
    elif [ -d c:/i/msys64 ]; then
        MSYS2PATH=c:/i/msys64
    fi
fi

if [ x"${1}" = x"pre" ]; then
    prebuild
elif [ x"${1}" = x"post" ]; then
    postbuild
else
    echo unknown step
    false
fi

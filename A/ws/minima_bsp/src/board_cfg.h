/* 参考 https://en-support.renesas.com/knowledgeBase/20025489 
 * 参考 EK-RA6M4向けにプロジェクトを作成したときの
 * ra_cfg/fsp_cfg/bsp/board_cfg.h
 */
#ifndef BOARD_CFG_H_
#define BOARD_CFG_H_

#include "../../../ra/board/ra4m1_minima/board.h"

#endif

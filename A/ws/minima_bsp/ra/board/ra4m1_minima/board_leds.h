/* 参考 https://en-support.renesas.com/knowledgeBase/20025489 
 * 参考 EK-RA6M4向けにプロジェクトを作成したときの
 * ra/board/ra6m4_ek/board_leds.h
 */
#ifndef BOARD_LEDS_H
#define BOARD_LEDS_H

#include <stdint.h> // uint16_t

#ifdef __cplusplus
extern "C" {
#endif

typedef struct st_bsp_leds
{
    uint16_t        led_count;
    uint16_t const* p_leds;
} bsp_leds_t;

/* ふつう、LEDはg_bsp_leds.p_leds[n]の形で参照されるため、
 * typedef enum e_bsp_led 〜 bsp_led_t は無くてもたいていは困らない。
 * よって、名前空間を汚さないためにあえて定義しないでおく。 */

extern const bsp_leds_t g_bsp_leds;

#ifdef __cplusplus
}
#endif
#endif

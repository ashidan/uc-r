/* 参考 https://en-support.renesas.com/knowledgeBase/20025489 
 * 参考 EK-RA6M4向けにプロジェクトを作成したときの
 * ra/board/ra6m4_ek/board_leds.h
 */

#include "bsp_api.h"
#if defined(BOARD_RA4M1_MINIMA)

static const uint16_t l_bsp_prv_leds[] = {
    (uint16_t)BSP_IO_PORT_01_PIN_11, //!< 回路図ではP111_GPT3_A
};

const bsp_leds_t g_bsp_leds = {
    .led_count = (uint16_t)((sizeof(l_bsp_prv_leds)/sizeof(l_bsp_prv_leds[0]))),
    .p_leds    = l_bsp_prv_leds
};
#endif

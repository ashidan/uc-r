/**
 * @file
 * @brief C++の挙動を確かめるための実験場
 * @copyright 2024 molelord
 * @see https://opensource.org/license/mit/
 */

#include "CppUTest/TestHarness.h"

// 配列のとある要素から末尾要素までのバイト数は？
TEST_GROUP(area)
{
    void setup()
    {
    }

    void teardown()
    {
    }
};

TEST(area, uint8array)
{
    uint8_t buf[32];
    uint8_t* end = &buf[32];

    void* oldptr = &buf[29];
    CHECK_TRUE(end - (uint8_t*)oldptr >= 0);
    const size_t tailBytes = (size_t)(end - (uint8_t*)oldptr);
    UNSIGNED_LONGS_EQUAL(3 * sizeof(uint8_t), tailBytes);
}
